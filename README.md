## Yet another dotfiles repo
---
Sep10
![sep10](preview/10-09-2016_1473523831_1920x1080.png)
---
Spring!
![spring](preview/07-05-2016_1462586826_1920x1080.png)
---
### Mount Setup
![mount](preview/mount.gif)

[Dotfiles](Mount/)
[Salt setup](salt/)

---
### Ci3namon Setup
![ci3namon](preview/ci3namon_02.png)
[DeviantArt](http://kalushary.deviantart.com/art/Ci3namon-553093610)

[Ceckout](ci3namon/)

---

### pInk Setup
![pInk](preview/pInk_01.png)

[Ceckout](pInk/)

---

### i3-cinnamon-session
Cinnamon DE with i3 window manager. Originaly made by [Gigadoc2](https://github.com/Gigadoc2/i3-cinnamon)

[Download .deb](repo/i3-cinnamon-session_0.1-3ubuntu2_all.deb)

---

### i3-exit
![i3exit](preview/i3exit.png)

Simple script to exit/logout with cinnamon-session-quit and mate-session-save.
Example usage:
```
bindsym $mod+o exec ~/bin/i3-cm-exit.sh -c logout
bindsym $mod+Shift+o exec ~/bin/i3-cm-exit.sh -c shutdown
```

[Checkout](bin/minimalenv/i3-cm-exit.sh)