#/bin/bash
C=0
for i in {16..255} ; do
    printf "\x1b[38;5;${i}m${i}\t"
    (( C++ ))
    if [[ $C -gt 17 ]]; then
      printf "\n"
    C=0
    fi
done
printf "\n"
for i in {0..15} ; do
    printf "\x1b[38;5;${i}m███ "
done
printf "\n"
