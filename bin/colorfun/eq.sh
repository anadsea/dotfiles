#!/bin/bash
#

. ~/.bash_colors

_DISTRO=$(cat /etc/lsb-release | grep DISTRIB_DESCRIPTION | awk -F"\"" '{print $2}')
_SESSION=$(echo $DESKTOP_SESSION)
_WM=$(xprop -id $(xprop -root | grep "_NET_SUPPORTING_WM_CHECK(" | awk '{print $5}') | grep "_NET_WM_NAME" | awk -F"\"" '{print $2}')
_GTKTHEME=$(gsettings get org.cinnamon.desktop.interface gtk-theme |sed "s/'//g")
_ICONTHEME=$(gsettings get org.cinnamon.desktop.interface icon-theme |sed "s/'//g")
_GTKFONT=$(gsettings get org.cinnamon.desktop.interface font-name |sed -e "s/'//g" -e 's/,//g')
_TERMFONT=$(gsettings get org.gnome.desktop.interface monospace-font-name|sed "s/'//g")



echo -e "




        $Red┇┇$IRed┇┇┇$Color_Off$Green┇┇┇$IGreen┇┇┇$Color_Off$Yellow┇┇┇$IYellow┇┇┇$Color_Off$Blue┇┇┇$IBlue┇┇┇$Purple┇┇┇$Color_Off$IPurple┇┇┇$Color_Off$Cyan┇┇┇$ICyan┇┇┇$Color_Off$White┇┇┇$IWhite┇┇┇$Color_Off
"
echo -e "\t"${Cyan}OS:$IWhite $_DISTRO${Color_Off}
echo -e "\t"${Cyan}WM:$IWhite $_WM${Color_Off}
echo -e "\t"${Cyan}Session:$IWhite $_SESSION${Color_Off}
echo -e "\t"${Cyan}GTK Theme:$IWhite $_GTKTHEME${Color_Off}
echo -e "\t"${Cyan}Icon Theme:$IWhite $_ICONTHEME${Color_Off}
echo -e "\t"${Cyan}GTK Font:$IWhite $_GTKFONT${Color_Off}
echo -e "\t"${Cyan}Terminal Font:$IWhite $_TERMFONT${Color_Off}
echo -e "
        $Red┇┇┇$IRed┇┇┇$Color_Off$Green┇┇┇$IGreen┇┇┇$Color_Off$Yellow┇┇┇$IYellow┇┇┇$Color_Off$Blue┇┇┇$IBlue┇┇┇$Purple┇┇┇$Color_Off$IPurple┇┇┇$Color_Off$Cyan┇┇┇$ICyan┇┇┇$Color_Off$White┇┇┇$IWhite┇┇┇$Color_Off"
echo -e "\n"
