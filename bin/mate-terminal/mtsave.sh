#!/bin/bash

PROFILE=Default
FILENAME="mate-terminal.bak"

usage() {
  echo -e "Usage:\n
  mtsave.sh backup <profilename> <filename>\n
  mtsave.sh restore <profilename> <filename>"
}

if [[ -z "$1" ]]; then
    usage
fi

if [[ -z "$2" ]]; then
    echo  "Set the name of your profile to backup or default will be used"
else
    PROFILE="$2"
fi

if [[ -z "$3" ]]; then
    echo  ""
else
    FILENAME="$3"
fi

backup() {
> $FILENAME
for i in $(gsettings list-keys org.mate.terminal.profile:/org/mate/terminal/profiles/$PROFILE/); do
        key=`gsettings get org.mate.terminal.profile:/org/mate/terminal/profiles/$PROFILE/ $i`;
        echo gsettings set org.mate.terminal.profile:/org/mate/terminal/profiles/Aesdana/ $i $key >> $FILENAME
done
echo "Saved to file mate-terminal.bak"
}

restore() {
cat $FILENAME | while read line; do
        echo $line
done
}


case $1 in
	backup) backup;;
	restore)restore;;
esac
