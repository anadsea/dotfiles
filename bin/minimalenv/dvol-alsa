#!/bin/bash

#Customize this stuff
IF="Master"
SECS="3"

BG="#191311"        # background colour of window
FG="#BA9E95"        # foreground colour of text/icon
BAR_FG="#3B7C87"    # foreground colour of volume bar
BAR_BG="#45342E"    # background colour of volume bar
WIDTH="250"
HEIGHT="30"
WIDTH_BAR=$(($WIDTH - 150))
WINDOW_WIDTH=$(xdpyinfo | awk '$1 ~ /dimensions/ {split($2,arr,"x"); print int(arr[1])}')
WINDOW_HEIGHT=$(xdpyinfo | awk '$1 ~ /dimensions/ {split($2,arr,"x"); print int(arr[2])}')
XPOS=$((($WINDOW_WIDTH / 2) - ($WIDTH / 2)))
YPOS=$(($WINDOW_HEIGHT / 2))
FONT="-*-terminus-*-r-normal-*-*-120-*-*-*-*-iso8859-*"                                                                                                                  


ICON_VOL=~/.icons/xbm/spkr_02.xbm
ICON_VOL_MUTE=~/.icons/xbm/spkr_02.xbm
ICON=$ICON_VOL

#Probably do not customize
PIPE="/tmp/dvolpipe"

err() {
  echo "$1"
  exit 1
}

usage() {
  echo "usage: dvol [option] [argument] [interface]"
  echo
  echo "Options:"
  echo "     -i, --increase - increase volume by \`argument'"
  echo "     -d, --decrease - decrease volume by \`argument'"
  echo "     -t, --toggle   - toggle mute on and off"
  echo "     -h, --help     - display this"
  echo
  echo "The interface could be specified by the interface"
  echo "parameter. If it is not specified, the IF var will be"
  echo "used."
  exit
}

#Argument Parsing
case "$1" in
  '-i'|'--increase')
    [ -z "$2" ] && err "No argument specified for increase."
    [ -n "$(tr -d [0-9] <<<$2)" ] && err "The argument needs to be an integer."
    AMIXARG="${2}%+"
    ;;
  '-d'|'--decrease')
    [ -z "$2" ] && err "No argument specified for decrease."
    [ -n "$(tr -d [0-9] <<<$2)" ] && err "The argument needs to be an integer."
    AMIXARG="${2}%-"
    ;;
  '-t'|'--toggle')
    AMIXARG="toggle"
    ;;
  ''|'-h'|'--help')
    usage
    ;;
  *)
    err "Unrecognized option \`$1', see dvol --help"
    ;;
esac

# Optional interface parameter:
if [ $AMIXARG == "toggle" ]; then
    IF=${2:-$IF}
else
    IF=${3:-$IF}
fi


#Actual volume changing (readability low)
AMIXOUT="$(amixer set "$IF" "$AMIXARG" | tail -n 1)"
MUTE="$(cut -d '[' -f 4 <<<"$AMIXOUT")"
if [ "$MUTE" = "off]" ]; then
  ICON=$ICON_VOL_MUTE
else
  ICON=$ICON_VOL
fi

VOL="$(cut -d '[' -f 2 <<<"$AMIXOUT" | sed 's/%.*//g')"

#Using named pipe to determine whether previous call still exists
#Also prevents multiple volume bar instances
if [ ! -e "$PIPE" ]; then
  mkfifo "$PIPE"
  (dzen2 -tw $WIDTH  -h "$HEIGHT" -x "$XPOS" -y "$YPOS" -fn "$FONT" -w "$WIDTH" -bg "$BG" -fg "$FG" < "$PIPE"; rm -f "$PIPE") &
fi

#Feed the pipe!
(echo "$VOL" | dzen2-gdbar -l "$IF ^i(${ICON}) " -fg "$BAR_FG" -bg "$BAR_BG" -w $WIDTH_BAR ; sleep "$SECS") > "$PIPE"
