#!/bin/bash

# Script for quick quit and logout dialogs in both mate and cinnamon
# Options are -m for Mate and -c for Cinnamon

CMD=""

case $1 in
    -c)
        CMD="cinnamon-session-quit"
        LOGOUT="--logout"
        SHUTDOWN="--power-off"
    ;;
    -m)
        CMD="mate-session-save"
	LOGOUT="--logout-dialog"
	SHUTDOWN="--shutdown-dialog"
    ;;
    *)
        echo "Use -m for Mate and -c for Cinnamon as first argument"
    ;;

esac


case $2 in 
    logout) 
        $CMD $LOGOUT
    ;;
    shutdown)
        $CMD $SHUTDOWN
    ;;
    *)
        echo "Specify action: logout or shutdown"
    ;;
esac
