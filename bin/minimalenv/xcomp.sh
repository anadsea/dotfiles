#!/bin/bash
#
# Start a composition manager.
# (xcompmgr in this case)

comphelp() {
    echo "Composition Manager:"
    echo "   (re)start: COMP"
    echo "   stop:      COMP -s"
    echo "   query:     COMP -q"
    echo "              returns 0 if composition manager is running, else 1"
    exit
}

check() {
    pgrep xcompmgr &>/dev/null
}

stop() {
    check && killall xcompmgr
}

start() {
    stop
    # Example settings only. Replace with your own.
    xcompmgr -Cc -t-3 -l-4 -r4 -o.7 &
    exit
}

case "$1" in
    "")   start ;;
    "-q") check ;;
    "-s") stop; exit ;;
    *)    comphelp ;;
esac
