#!/bin/bash

ZEN=$(which zenity)
NOT=$(which notify-send)
SCROT=$(which scrot)
IMG='%d-%m-%Y_%s_$wx$h.png'

f_choose () {
	OUT=$($ZEN  --list  --text "Screenshot" --radiolist  --column "Pick" --column "Action" TRUE "Fullscreen" FALSE "Window" FALSE "Selection" FALSE "Delay" --height=230)
}

f_choose

case $OUT in
	Fullscreen) sleep 1s; $SCROT "$IMG" -e 'mv $f ~/Pictures/Shots/' && $NOT  -u normal -t 4 "Screenshot captured"
		;;
	Window) sleep 1s; $SCROT -u "$IMG" -e 'mv $f ~/Pictures/Shots/' && $NOT -u normal -t 4 "Screenshot captured"
		;;
	Selection) sleep1s; $SCROT -s "$IMG" -e 'mv $f ~/Pictures/Shots/' && $NOT -u normal -t 4 "Screenshot captured"
		;;
	Delay) $SCROT -d 5 "$IMG" -e 'mv $f ~/Pictures/Shots/' && $NOT -u normal -t 4 "Screenshot captured"
		;;
esac

f_jing () {
	SOURCE=$(cd /home/aesdana/Pictures/Shots; zenity  --file-selection --file-filter "*.png")
	DEST=$(zenity --entry --text="Set name for remote file" --entry-text="upload")
	jing $SOURCE ${DEST}.png
}

if [[ "$1" == "jing" ]]; then
	f_jing
fi
