#!/bin/bash
. $HOME/.config/panel/panelrc

makebar() {
    MAX=10
    CUR=$(($1 / 10))

for v in $(seq 0 $((MAX - 1))); do
    if [[ "$v" -lt "$CUR" ]]; then
        bar="${bar}${yellow}${volbar}"
    else
        bar="${bar}${fg}${volbar}"
    fi
done

echo "${bar}"
}

work(){
ws=""
line=$(bspc control --get-status)
for i in $(echo $line | cut -d':' -f2-7 | tr ':' '\n')
    do
        case $i in
            [OFU]*)
            # active desktop
            ws="${ws}${fg}   " ;;
            o*)
            # inactive but occupied desktop
            ws="${ws}${fg}   " ;;
            f*)
            # inactive desktop
            ws="${ws}${fg}   " ;;
            u*)
            # urgent desktop
            ws="${ws}${red}   " ;;
        esac
    done
echo "${ws}"
}

layout() {
ICON=""
echo -e $ICON $(xkblayout-state print %s)
}

clock() {
ICON=""
echo -e $ICON $(date '+%H:%M')
}

cal() {
ICON=""
echo -e $(date '+%A, %d %B')
}

mem () {
ICON=""
MEM=$(free -m | awk '/Mem/ {printf "%5.0f", $3 / ($2 / 100) }' | sed 's/ //g')
if [[ $MEM -gt "90" ]]; then
    echo -e ${red} $ICON $MEM"%"
elif [[ $MEM -ge "75" ]] && [[ $MEM -le "90" ]]; then
    echo -e ${orange} $ICON $MEM"%"
else
    echo -e ${fg} $ICON $MEM"%"
fi
}

cpu () {
ICON=""
CPU=$(sensors | grep "Core 0" | awk '{print $3}' | tr -d +)
echo $ICON $CPU
}

volume () {
echo -e $(amixer -c 1 get Master | grep Mono: | awk '{print $4}'| tr -d "[]%")
}

wifi () {
ICON=""
echo -e $ICON $(iwconfig wlan0 | grep "ESSID" | awk  'BEGIN{FS=":"}{print $2}'| tr -d \")
}

battery () {
PERC=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | awk '/percentage:/ {gsub(/%$/,""); print $2}')
STATE=$(upower -i /org/freedesktop/UPower/devices/battery_BAT0 | awk '/state:/ {print $2}')

if [[ $STATE = "fully-charged" ]] || [[ $STATE = "charging" ]]; then
    echo -e ${green}"" ${PERC}"%"
elif [[ $STATE = "discharging" ]]; then
    if [[ $PERC -ge "0" ]] && [[ $PERC -le "15" ]]; then
        echo -e ${red}"" ${PERC}"%"
    elif [[ $PERC -ge "16" ]] && [[ $PERC -le "25" ]]; then
        echo -e ${orange}"" ${PERC}"%"
    elif [[ $PERC -ge "26" ]] && [[ $PERC -le "50" ]]; then
        echo -e ${yellow}"" ${PERC}"%"
    elif [[ $PERC -ge "51" ]] && [[ $PERC -le "75" ]]; then
        echo -e ${green}"" ${PERC}"%"
    elif [[ $PERC -ge "76" ]] && [[ $PERC -le "100" ]]; then
        echo -e ${green}"" ${PERC}"%"
    fi
fi

}


sep() {
echo "         "
}

infotoggle() {
if [[ $(cat /tmp/infotoggle) = "0" ]]; then
    echo -e "%{A:bartoggle.sh info:} %{A}"
else
    echo -e "${fg} $(mem)  ${fg}$(cpu) %{A:bartoggle.sh info:} %{A}"
fi

}

caltoggle() {
if [[ $(cat /tmp/caltoggle) = "0" ]]; then
    echo -e "%{A:bartoggle.sh cal:}  %{A}"
else
    echo -e "%{A} %{A:bartoggle.sh cal:} %{A} ${fg}%{A:calendar.sh:}$(cal)  "
fi
}

termrun() {
echo -e "%{A:mate-terminal:} %{A}"
}

sxhkdrun() {
echo -e "%{A:[[ $(pgrep -f sxhkd) ]] && pgrep -f sxhkd | xargs kill -9 && sxhkd:}   %{A}"	
}

touch /tmp/infotoggle; touch /tmp/caltoggle

while :; do
echo -e  "%{l} \
${fg}$(sep)$(sep)$(sep) $(termrun)\
  $(clock)  ${fg} $(caltoggle) \
%{l}\
%{c}\
$(work)\
%{c}\
%{r}\
  $(infotoggle)\
    $(battery)\
  ${yellow}  ${fg}$(makebar $(volume))    \
${bg}\
%{r}"
sleep 0.5s
done
