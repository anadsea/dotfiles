#!/bin/bash
. $HOME/.config/panel/panelrc

if [ $(pgrep -cx bar) -gt 0 ] ; then
       printf "%s\n" "The panel is already running. Killing previous one" >&2

       kill $(pgrep -f "bar -p") $(pgrep -f stalonetray)
fi

${FEED}| ${BAR} -p -F ${FG} -B ${BG} -g ${GEOMETRY} -f ${FONT1} -f ${FONT2} -f ${FONT3} -f ${FONT4} | bash &
sleep 1s
stalonetray -c $HOME/.config/stalonetray/stalonetrayrc &
