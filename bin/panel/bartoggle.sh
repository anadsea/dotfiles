#! /bin/bash

# Make sure the following file exists.
FILE="/tmp/${1}toggle"

[[ $(< $FILE) -eq 0 ]] && echo -n 1 > $FILE || echo -n 0 > $FILE
