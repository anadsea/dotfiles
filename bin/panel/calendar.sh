. ~/bin/panel/panelrc
TODAY=$(expr `date +'%d'` + 0)
MONTH=`date +'%m'`
YEAR=`date +'%Y'`
(
date +''
date +''
cal -h  | \
sed -r \
-e "1,2 s/.*/^fg(${LIGHT2})&^fg()/" \
-e "s/(^| )($TODAY)($| )/\1^bg(${BG})^fg(${ORANGE})\2^fg()^bg()\3/") | \
dzen2 -p 10 -e \
'onstart=uncollapse,hide;leavetitle=exit;button3=exit;button1=exit;' \
-fg ${LIGHT4} -bg ${BG} -fn ${FONT1}\
  -x '260' -y '3' -w '200' -l '9' -sa c
