-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: i3-wm
Binary: i3, i3-wm, i3-wm-dbg
Architecture: any
Version: 4.7.2-1
Maintainer: Michael Stapelberg <stapelberg@debian.org>
Homepage: http://i3wm.org/
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 7.0.50~), libx11-dev, libxcb-util0-dev (>= 0.3.8), libxcb-keysyms1-dev, libxcb-xinerama0-dev (>= 1.1), libxcb-randr0-dev, libxcb-icccm4-dev, libxcb-cursor-dev, asciidoc (>= 8.4.4), xmlto, docbook-xml, pkg-config, libev-dev (>= 1:4.04), libyajl-dev, libpcre3-dev, libstartup-notification0-dev (>= 0.10), libcairo2-dev, libpango1.0-dev, libpod-simple-perl
Package-List: 
 i3 deb x11 extra
 i3-wm deb x11 extra
 i3-wm-dbg deb debug extra
Checksums-Sha1: 
 a324170f0fee581fd1f34549eda0800cab985ad8 897398 i3-wm_4.7.2.orig.tar.bz2
 56e40d3f935a0f6d4c4ded7273e7280ba744af4d 12592 i3-wm_4.7.2-1.debian.tar.xz
Checksums-Sha256: 
 6fe4565e364a5017eb6a89ce104d6bb21afcdb4bb0b3bee9526781fa64b1f393 897398 i3-wm_4.7.2.orig.tar.bz2
 104e280b525b9d9a6ac57375087335c7f9efa6e06de15ee4664806b99a4a378d 12592 i3-wm_4.7.2-1.debian.tar.xz
Files: 
 64141f7c23f97cd1e52c52918476c1c8 897398 i3-wm_4.7.2.orig.tar.bz2
 e30545d9a0e7e0fa04fbdd384f509645 12592 i3-wm_4.7.2-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBCgAGBQJS4ZHrAAoJEE5xYO1KyO4dsXQQANaiH2nozdTJ/iSsHDukR2GY
L4eUhA3F9G4GqGb6w+/lLUSxCvQzggACADprYsXF5Lwtbj8ceeIkM3Ew4/EMVM0j
ptDZ71qKITyxa+iYH9dCOnAhK+1zpzsu98JEz86Vr8lsMZbtrT5Ui+gq6+D4vld9
zVPhw9nIkNdjR+8xYbhvvwD9zns1ccxySasxVjTR6om0l6bgEaUcE1la4b8w6YcO
gjqqMunMlNa0njVfurY6Xi8H7KKi86YIiazWcyKYckO2ELu/yXCJ42H3bnp1vQKD
3Lh0VTE3BCczEZ69wGR5ApiKKmqo6JFdIyYwn5xraWjR3H+uLphmSOq608fNTGx2
xAbRFB3GIAL46gYdzXQHzpY0Wm7xuKtQ8ScPvRyJ5/u1zd10kDqnEG4zcDgFgvUD
oU8rawhogzXid8BeEd+EQR0YandGtIFZojlrGMUcXLR2C2tytXzwFxcXGa3EfY/g
AaK6XhEu1qj34THfQi7bs6T355SaVcN/y51I5GGSBH0/SKlfgE4QjZ/qGlkRzskM
D9yUGY0zQvg+gHVZPNDZnsq/+PUpKnRCGpHdeFi9plroic/vcI1CpRGfvpuzyWrm
1hR+Lxpo3AYVsEO5taLaD6HFX598eXhbxUO0290ATxITcReWQ1KsFIg6onrfyl59
C8xL1ku1/ItFxTtu562P
=+atz
-----END PGP SIGNATURE-----
