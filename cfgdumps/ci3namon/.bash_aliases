#!/bin/bash
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'
alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias egrep='egrep --color=auto'
alias cp="cp -v"      # interactive, verbose
alias rm="rm -i"      # interactive
alias mv="mv -iv"       # interactive, verbose
alias grep="grep -i --color=auto"  # ignore case
alias mkdir='mkdir -pv'

alias rmsp="for i in * * ; do mv '$i' '${i// /_}' ; done"
alias xctr="export TERM=xterm; LANG=C executer --cached"

alias yandexrepooff='for repo in unstable yandex prestable verstka; do if [[ -e /etc/apt/sources.list.d/"$repo".list ]]; then sudo mv /etc/apt/sources.list.d/"$repo".list /etc/apt/sources.list.d/"$repo".off && echo "$repo" off; fi; done'
alias yandexrepoon='for repo in unstable yandex prestable verstka; do if [[ -e /etc/apt/sources.list.d/"$repo".off ]]; then sudo mv /etc/apt/sources.list.d/"$repo".off /etc/apt/sources.list.d/"$repo".list && echo "$repo" on; fi; done'

alias diff='colordiff'
alias breload='source ~/.bashrc'
alias vpn="sudo /etc/init.d/openvpn restart"

alias tm="tmux -2 attach -t aesdana"

alias ai="sudo apt-get install"
alias ap="sudo apt-get purge"
alias af="sudo apt-get -f install"
alias an="sudo apt-get install --no-install-recommends "
