""" MAIN
set t_Co=256
syntax on
set laststatus=2
set background=dark
set nocompatible 
filetype off 

""" VUNDLE
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gruvbox'
call vundle#end()            " required
filetype plugin indent on    " required

""" COLORSCHEME
colorscheme gruvbox

""" STATUSLINE
set statusline=%f                           " file name
set statusline+=[%{strlen(&fenc)?&fenc:'none'}, "file encoding
set statusline+=%{&ff}] "file format
set statusline+=%y      "filetype
set statusline+=%h      "help file flag
set statusline+=%m      "modified flag
set statusline+=%r      "read only flag
set statusline+=\ %=                        " align left
set statusline+=Line:%l/%L[%p%%]            " line X of Y [percent of file]

nmap OH 0
nmap OF $
