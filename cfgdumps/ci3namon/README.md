## Ci3namon Setup

- **OS:** Mint 17.1 Rebecca
- **Resolution:** 1366x768
- **DE:** Cinnamon 2.4.8
- **WM:** i3
- **GTK Theme:** [Numix Champagne GTK2/3](https://xaahudude.deviantart.com/art/Numix-Champagne-GTK3-Theme-404646207)
- **Icon Theme:** [Ardis Mega Icon Theme](https://nitrux.in/store/ardis-icon-theme/)
- **Font:** [Yanone Kaffeesatz](https://www.yanone.de/typedesign/kaffeesatz/)
- **Notification daemon:** [dunst](http://www.knopwob.org/dunst/)
- **App launcher:** dmenu & [j4-dmenu-desktop](http://www.j4tools.org/)
- **Desktop locker:** [i3lock](http://i3wm.org/i3lock/)

Preview (old)

![ci3namon_01](preview/ci3namon_01.png)

Preview (current)

![ci3namon_02](preview/ci3namon_02.png)

[Original Image 1920x1080](preview/aesdana_ci3namon_1920x1080.png)

---

### SimpleDark MDM theme
![simpledark](themes/mdm/SimpleDark/screenshot.jpg)

Based on SimpleGreeter theme

[Ceckout](themes/mdm/SimpleDark/)