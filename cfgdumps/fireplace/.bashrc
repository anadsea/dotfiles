if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi

if [ -f ~/.bash_colors ]; then
    . ~/.bash_colors
fi

if [ -f ~/.dircolors ] ; then
    eval $(dircolors -b ~/.dircolors)
fi

if [ -f ~/.bash_yandex ]; then
    . ~/.bash_yandex
fi

PATH="$HOME/bin/sutils:$HOME/bin/panel:$HOME/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

GIT_PROMPT_ONLY_IN_REPO=1
source ~/bin/bash-git-prompt/gitprompt.sh

#source ~/.vim/gruvbox/gruvbox_256palette.sh

[ -z "$PS1" ] && return

PS1=" \[${Yellow}\]\u@\h\[${White}\]: "

HISTCONTROL=ignoredups:ignorespace
HISTSIZE=30000
HISTFILESIZE=20000
color_prompt=yes
shopt -s checkwinsize
shopt -s histappend
shopt -s dirspell
shopt -s cdable_vars
shopt -s cmdhist
shopt -s globstar

#http_proxy=http://127.0.0.1:8118/
#HTTP_PROXY=$http_proxy
#export http_proxy HTTP_PROXY

/usr/bin/keychain ~/.ssh/id_rsa
/usr/bin/keychain ~/.config/gitlab/aesdana
[[ -f $HOME/.keychain/$HOSTNAME-sh ]] && source $HOME/.keychain/$HOSTNAME-sh

export GPG_AGENT_INFO  # the env file does not contain the export statement
export SSH_AUTH_SOCK   # enable gpg-agent for ssh

export TERM="screen-256color"
export EDITOR="vim"
export SHMUX_SSH_OPTS='-qxa'
export LC_USER="aesdana"
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

if [[ -f Work/os-rc-gen ]]; then
    bash Work/os-rc-gen > ~/.bash_os
    source ~/.bash_os
fi

export PATH=${PATH}:~/android-sdk-linux/tools
export PATH=${PATH}:~/android-sdk-linux/platform-tools

LD_LIBRARY_PATH="${LD_LIBRARY_PATH:+$LD_LIBRARY_PATH:}/usr/local/lib"
export LD_LIBRARY_PATH
