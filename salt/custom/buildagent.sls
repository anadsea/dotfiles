{% set codename = grains.lsb_distrib_codename %}

{% if 'buildagent' in pillar %}
{% set teamcity_server = salt.pillar.get('buildagent:server', 'localhost') %}
{% set teamcity_home = salt.pillar.get('buildagent:teamcity_homedir', '/home/teamcity') %}
{% set agent_home = salt.pillar.get('buildagent:agent_homedir', '/usr/local/teamcity-agents/local') %}
{% set agent_work = salt.pillar.get('buildagent:agent_workdir', '/var/lib/teamcity/BuildAgents/local' ) %}

{% if codename == 'trusty' %}
/etc/pam.d/sshd-buildagent:
  file.replace:
    - name: /etc/pam.d/sshd
    - pattern: '^session    required     pam_loginuid.so'
    - repl: '#session    required     pam_loginuid.so'
    - count: 1
{% endif %}

agent_kvm_dev:
  file.managed:
    - name: /dev/kvm
    - mode: 666

/etc/security/limits.d/teamcity.conf:
  file.managed:
    - template: jinja
    - contents:  |
        teamcity soft nofile 65536
        teamcity hard nofile 65536
        root     soft nofile 65536
        root     hard nofile 65536

# This is for g++-4.9
/etc/apt/sources.list.d/ubuntu-toolchain-r-test-trusty.list:
  file.managed:
    - template: jinja
    - contents:  |
        deb http://ppa.launchpad.net/ubuntu-toolchain-r/test/ubuntu trusty main
        #deb-src http://ppa.launchpad.net/ubuntu-toolchain-r/test/ubuntu trusty main

# Cleanup work directory every night
agent_cron:
  file.managed:
    - user: root
    - name: /etc/cron.d/teamcity-agent
    - contents:  |
            0   3 * * *   root      find {{ agent_work }}/work/ -type f -mtime +5 -delete

# Packages setup
agent_pkgs:
  pkg.installed:
   - pkgs: {{ salt.pillar.get('buildagent:pkgs', '') }}

# init.d/ script setup
agent_init:
  file.managed:
    - name: /etc/init.d/teamcity-agent
    - source: salt://files/buildagent/teamcity-agent
    - mode: 755

# recursively copy prepared directories
teamcity_home:
  file.recurse:
    - name: /home/teamcity/
    - source: salt://files/buildagent/teamcity_home
    - file_mode: 644
    - makedirs: true
    - user: teamcity

teamcity_home_gnupg:
  file.recurse:
    - name: /home/teamcity/.gnupg
    - source: salt://files/buildagent/teamcity_home_gnupg
    - file_mode: 600
    - dir_mode: 700
    - makedirs: true
    - user: teamcity

{% for name in salt.pillar.get('buildagent:ssh_keys') %}
/home/teamcity/.ssh/{{ name }}:
  file.managed:
    - template: jinja
    - mode: 600
    - makedirs: true
    - user: teamcity
    - contents_pillar: buildagent:ssh_keys:{{ name }}
{% endfor %}

# get needed sdk version from pillar and prepare paths
{% set sdk_version = salt.pillar.get('buildagent:sdk_version', 'r24.4.1') %}
{% set sdk_file_name = 'android-sdk_' + sdk_version + '-linux.tgz' %}
{% set sdk_path = teamcity_home + '/android-sdk-linux' %}

# download sdk
sdk_get:
  cmd.run:
    - unless: ls {{ teamcity_home }}/{{ sdk_file_name }}
    - creates: {{ teamcity_home }}/{{ sdk_file_name }}
    - name: curl -s -o {{ teamcity_home }}/{{ sdk_file_name }} http://dl.google.com/android/{{ sdk_file_name }}
    - user: teamcity

# exctact sdk
sdk_extract:
   cmd.run:
    - unless: ls {{ sdk_path }}
    - creates: {{ teamcity_home }}/{{ sdk_path }}
    - name: cd {{ teamcity_home }}; mkdir {{ sdk_path }}; tar -xzf {{ sdk_file_name }}
    - user: teamcity

{{ teamcity_home }}/ndk:
  file.directory:
    - user: teamcity
    - makedirs: True 

{{ teamcity_home }}/android-sdk-linux:
  file.symlink:
    - name: {{ teamcity_home }}/android-sdk-linux-auto
    - target: {{ teamcity_home }}/android-sdk-linux
    - user: teamcity
    - backupname: {{ sdk_path }}_renamed_by_salt

# go through ndk versions dict, download self-extracted binaries, extract them
# for fresh ndk versions:
{% for ndk_version in salt.pillar.get('buildagent:ndk_version', '') %}
{% set ndk_dir_name = 'android-ndk-' + ndk_version %}
{% set ndk_file_name = ndk_dir_name + '-linux-x86_64.bin' %}

ndk_get_{{ ndk_version }}:
  cmd.run:
    - onlyif: ls {{ teamcity_home }}
    - creates: {{ teamcity_home }}/{{ ndk_file_name }}
    - name: curl -s -o {{ teamcity_home }}/{{ ndk_file_name }} http://dl.google.com/android/ndk/{{ ndk_file_name }}; chmod +x {{ teamcity_home }}/{{ ndk_file_name }}
    - user: teamcity

ndk_exctract_{{ ndk_version }}:
  cmd.run:
    - onlyif: ls {{ teamcity_home }} && ls {{ teamcity_home }}/{{ ndk_file_name }}
    - creates: {{ teamcity_home }}/{{ ndk_dir_name }}
    - name: cd {{ teamcity_home }}; ./{{ ndk_file_name }}
    - user: teamcity

{{ teamcity_home }}/ndk/{{ ndk_dir_name }}:
  file.symlink:
    - target: {{ teamcity_home }}/{{ ndk_dir_name }}

{% endfor %}

# for legacy ndk versions (google, I hate you)
{% for legacy_ndk_version in salt.pillar.get('buildagent:legacy_ndk_version', '') %}
{% set ndk_dir_name = 'android-ndk-' + legacy_ndk_version %}
{% set ndk_file_name = ndk_dir_name + '-linux-x86_64.tar.bz2' %}
    
ndk_get_{{ legacy_ndk_version }}:
  cmd.run:
    - onlyif: ls {{ teamcity_home }}
    - creates: {{ teamcity_home }}/{{ ndk_file_name }}
    - name: curl -s -o {{ teamcity_home }}/{{ ndk_file_name }} http://dl.google.com/android/ndk/{{ ndk_file_name }}; tar xvjf  {{ teamcity_home }}/{{ ndk_file_name }}
    - user: teamcity

{{ teamcity_home }}/ndk/{{ ndk_dir_name }}:
  file.symlink:
    - target: {{ teamcity_home }}/{{ ndk_dir_name }}
  
{% endfor %}

# download fresh buildAgent.zip 
agent_get:
  cmd.run:
    - name: curl -s -o "/tmp/buildAgent.zip" "https://{{ teamcity_server }}/update/buildAgent.zip"

agent_extract:
  archive.extracted:
    - onlyif: ls /tmp/buildAgent.zip
    - name: {{ agent_home }}
    - source: /tmp/buildAgent.zip
    - archive_format: zip
    - user: teamcity

{{ agent_home }}/bin:
  file.directory:
    - onlyif: ls {{ agent_home }}
    - dir_mode: 775
    - file_mode: 755
    - user: teamcity
    - group: root
    - makedirs: True
    - recurse:
        - mode

{{ agent_home }}/launcher/bin:
  file.directory:
    - onlyif: ls {{ agent_home }}
    - dir_mode: 775
    - file_mode: 755
    - user: teamcity
    - group: root
    - makedirs: True
    - recurse:
        - mode

agent_properties:
  file.managed:
    - onlyif: ls {{ agent_home }}
    - name: {{ agent_home }}/conf/buildAgent.properties
    - source: salt://files/buildagent/buildAgent.properties
    - template: jinja
    - user: teamcity
    - makedirs: true
    - replace: false

agent_properties_ndk:
  file.blockreplace:
    - name: {{ agent_home }}/conf/buildAgent.properties
    - marker_start: "# START salt managed zone"  
    - marker_end: "# END salt managed zone"
    - append_if_not_found: True
    - show_changes: True
    - source: salt://files/buildagent/buildAgent.properties.append

/var/lib/teamcity:
  file.directory:
    - dir_mode: 777
    - user: teamcity
    - makedirs: True

{{ agent_work }}:
  file.directory:
    - user: teamcity
    - group: root
    - dir_mode: 777
    - file_mode: 777
    - makedirs: True

# Here madness comes
# Dirty hack: run agent for update, stop, add JAVA_OPTIONS and start again. Don't ask me why. BECAUSE.
agent_start:
  cmd.run:
    - unless: ls {{ agent_home }}/.created_by_first_start
    - name: /etc/init.d/teamcity-agent start local
    - creates: {{ agent_home }}/.created_by_first_start

watch_update:
  cmd.run:
    - name: while true; do [[ -f {{ agent_home }}/logs/upgrade.log ]] && break; done

agent_update:
  cmd.run:
    - unless: ls {{ agent_home }}/.was_updated
    - name: sleep 15s && tail -n20 {{ agent_home }}/logs/upgrade.log | grep -q "Finish upgrade at" && touch {{ agent_home }}/.was_updated && /etc/init.d/teamcity-agent restart local && break

opts_append:
  file.append:
    - unless: grep JAVA_OPTIONS {{ agent_home }}/.teamcityrc
    - name: {{ teamcity_home }}/.teamcityrc
    - text: export _JAVA_OPTIONS="-Dnetworkaddress.cache.ttl=120 -Djava.net.preferIPv6Addresses=true -Djava.net.preferIPv6Stack=true"

{% endif %}

# DOM0 SETUP

{% if 'buildfarm' in pillar %}
{% for file in salt['cmd.run']('ls /var/lib/lxc/').splitlines() %}

/var/lib/lxc/{{ file }}/config:
  file.replace:
    - pattern: ".*10:232.*"
    - repl: "lxc.cgroup.devices.allow = c 10:232 rwm          #   the /dev/kvm"
    - count: 1
    - append_if_not_found: True
    - backup: '.bak'
{% endfor %}

farm_kvm_module:
  kmod.present:
    - name: kvm_intel

farm_kvm_dev:
  file.managed:
    - name: /dev/kvm
    - mode: 666
{% endif %}
