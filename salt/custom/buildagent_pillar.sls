buildagent:
  server: teamcity_server_hostname.example.com
  own_port: 9090
  agent_workdir: /var/lib/teamcity/BuildAgents/local
  agent_homedir: /usr/local/teamcity-agents/local
  teamcity_homedir: /home/teamcity
  sdk_path: android-sdk-linux-auto
  ndk_version: [ r10e, r10d ]
  legacy_ndk_version: [ r9c ]
  sdk_version: r24.4.1
  pkgs: [ lib32stdc++6, git, subversion, mercurial, zip, ant, g++-4.9, gcc, python-virtualenv ]
  ssh_keys:
    id_rsa: |
      -----BEGIN DSA PRIVATE KEY-----
      ...
      -----END DSA PRIVATE KEY-----
    id_rsa.pub:  |
      ssh-rsa ... 
