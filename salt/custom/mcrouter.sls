{%- if 'mcrouter' in pillar -%}

{%- for instance in salt['pillar.get']('mcrouter:instances') -%}
  {%- set current_dc = salt.pillar.get('mcrouter:my_current_dc') -%}
  {%- set poollist = salt.pillar.get('mcrouter:' + instance + ':pools').items() -%}
  {%- set ordered_set_list = salt.pillar.get('mcrouter:' + instance + ':ordered_set_list:' + current_dc ) -%}
  {%- set ordered_get_list = salt.pillar.get('mcrouter:' + instance + ':ordered_get_list:' + current_dc ) -%}
  {%- set extra_set_pool = salt.pillar.get('mcrouter:' + instance + ':extra_set_pool') -%}
  {%- set extra_get_pool = salt.pillar.get('mcrouter:' + instance + ':extra_get_pool') -%}
  {%- set route_set_type = salt.pillar.get('mcrouter:' + instance + ':route_set_type', "AllFastestRoute") -%}
  {%- set route_get_type = salt.pillar.get('mcrouter:' + instance + ':route_get_type', "AllFastestRoute") -%}
  {%- set memcached_port = salt.pillar.get('mcrouter:' + instance + ':memcached_port', "11211") -%}
  {%- set mcrouter_port = salt.pillar.get('mcrouter:' + instance + ':mcrouter_port', "5000") -%}
  {%- set num_proxies = salt.pillar.get('mcrouter:' + instance + ':num_proxies', "4") -%}
# main configuration file
{{ instance }}_conf:
  file.managed:
    - name: /etc/mcrouter/{{ instance }}.conf
    - template: jinja
    - makedirs: true
    - user: root
    - group: root
    - mode: 0775
    - contents: |
        {
        "pools": {
        {%- for dc, hosts in poollist -%}
                "{{ dc }}": {
                        "servers": [ 
          {%- for host in hosts -%}
                                "{{ host }}:{{ memcached_port }}",
          {%- endfor -%}
                                ],
                },
        {%- endfor -%}

        {%- if  salt['pillar.get']('mcrouter:' + instance + ':extra_set_pool') -%}
                "custom_set_pool": {
                        "servers": [ 
          {%- for custom_host in salt['pillar.get']('mcrouter:' + instance + ':extra_set_pool') -%}
                                "{{ custom_host }}:{{ memcached_port }}",
          {%- endfor -%}
                                ],
                },
        {%- endif -%}

        {%- if  salt['pillar.get']('mcrouter:' + instance + ':extra_get_pool') -%}
                "custom_get_pool": {
                        "servers": [ 
          {%- for custom_host in salt['pillar.get']('mcrouter:' + instance + ':extra_get_pool') -%}
                                "{{ custom_host }}:{{ memcached_port }}",
          {%- endfor -%}
                                ],
                },
        {%- endif -%}

        },
        "route": {
                "type": "OperationSelectorRoute",
                "operation_policies": {
                        "set": {
                                "type": "{{ route_set_type }}",
                                "children": [
        {% if salt['pillar.get']('mcrouter:' + instance + ':set_only_current_dc') == True %}
                                        "PoolRoute|{{ current_dc }}",
        {%- else -%}
          {%- if salt['pillar.get']('mcrouter:' + instance + ':ordered_set_list') -%}
            {% for ordered_dc in ordered_set_list %}
                                        "PoolRoute|{{ ordered_dc }}",
            {% endfor %}
          {%- else -%}
          {%- for dc, hosts in poollist -%}
              {% if dc != current_dc %}
                                        "PoolRoute|{{ dc }}",
              {% endif %}
            {%- endfor -%}
          {%- endif -%}
        {%- endif -%}

        {% if  salt['pillar.get']('mcrouter:' + instance + ':extra_set_pool') %}
                                        "PoolRoute|custom_set_pool",
        {% endif %}
                                ]
                        },
                        "get": {
                                "type": "{{ route_get_type }}",
                                "children": [
        {%- if salt['pillar.get']('mcrouter:' + instance + ':get_only_current_dc') == True -%}
                                        "PoolRoute|{{ current_dc }}",
        {%- else -%}
          {% if salt['pillar.get']('mcrouter:' + instance + ':ordered_get_list') %}
            {% for ordered_dc in ordered_get_list %}
                                        "PoolRoute|{{ ordered_dc }}",
            {% endfor %}
          {%- else -%}
            {%- for dc, hosts in poollist -%}
              {% if dc != current_dc %}
                                        "PoolRoute|{{ dc }}",
              {% endif %}
            {%- endfor -%}
          {%- endif -%}
        {%- endif -%}

        {% if  salt['pillar.get']('mcrouter:' + instance + ':extra_get_pool') %}
                                        "PoolRoute|custom_get_pool",
        {% endif %}
                                ]
                        }
                }
        }
        }
# init script
{{ instance }}_init:
  file.managed:
    - name: /etc/init/{{ instance }}.conf
    - source: salt://files/mcrouter/mcrouter-init
    - template: jinja
    - makedirs: true
    - user: root
    - group: root
    - mode: 0775
    - context:
      mcr_name: {{ instance }}
      mcr_port: {{ mcrouter_port }}
      mcr_num_proxies: {{ num_proxies }}

{%- endfor -%}
{%- endif -%}

