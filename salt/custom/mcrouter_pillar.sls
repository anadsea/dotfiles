mcrouter:
  - instances: [ mcrouter, mcrouter_5001 ]
  - mcrouter_5001:
      - mcrouter_port: 5001
        num_proxies: 4
        memcached_port: 11211
        route_set_type: "AllFastestRoute"
        route_get_type: "AllFastestRoute"
        pools:
          dc1: [ host1, host2, host3 ]
          dc2: [ host1, host2, host3 ]
          dc3: [ host1, host2, host3 ]
        set_only_current_dc: False
        get_only_current_dc: False

  - mcrouter:
      - mcrouter_port: 5000
        num_proxies: 4
        memcached_port: 11211
        route_set_type: "AllFastestRoute"
        route_get_type: "AllFastestRoute"
        #        extra_set_pool: [ customhost1, customhost2 ]
        pools: 
          dc1: [ host1, host2, host3 ]
          dc2: [ host1, host2, host3 ]
          dc3: [ host1, host2, host3 ]
          dc4: [ host1, host2, host3 ]
        set_only_current_dc: False
        get_only_current_dc: True
        ordered_set_list:
          dc1: [ dc1, dc2, dc3 ]
          dc2: [ dc1, dc2, dc3  ]
          dc3: [ dc1 ]
          dc4: [ dc2  ]
#        ordered_get_list:
#          dc1: [ dc1, dc2, dc3 ]
#          dc2: [ dc1, dc2, dc3  ]

