#!/bin/bash
pkill -u "$USER" -USR1 dunst
LOCK="/home/aesdana/bin/lock.png"
# All options are here: http://www.imagemagick.org/Usage/blur/#blur_args
#BLURTYPE="0x5" # 7.52s
#BLURTYPE="0x2" # 4.39s
#BLURTYPE="5x3" # 3.80s
BLURTYPE="2x4" # 2.90s
#BLURTYPE="2x3" # 2.92s

~/bin/minimalenv/i3lock --textcolor={{ bg }}ff --insidecolor=ffffff00 --ringcolor={{ bg }}ff --linecolor=ffffff00 --keyhlcolor={{ fg }}ff --ringvercolor=00000000 --insidevercolor=00000000 --ringwrongcolor={{ urgent }}ff --insidewrongcolor=00000000  -i $LOCK

rm $IMAGE
pkill -u "$USER" -USR2 dunst
