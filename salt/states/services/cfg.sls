{% set HOME = "/home/aesdana" %}

{% for APP in "panel","stalonetray","bspwm","dunst","sxhkd" %}
{{ HOME }}/.config/{{ APP }}/{{ APP }}rc:
  file.managed:
    - source: salt://files/{{ APP }}rc
    - template: jinja
    - user: aesdana
    - group: aesdana
    - mode: 755
    - makedirs: true
    - defaults:
        terminal: "mate-terminal"
        browser: "opera"
        messenger: "Telegram" 
        fg: "A7ADBA"
        bg: "2B303B"
        accent: "96B5B4"
        orange: "D08770"
        red: "AD5860"
        green: "A3BE8C"
        yellow: "EBCB8B"
        blue: "8FA1B3"
        cyan: "96B5B4"
        urgent: "AD5860"
        lightaccent: "65737E"
        light4: "BAB2AC"
        bg_shade: "65737E"
        bg_shade_darker: "65737E"
        fg_shade: "65737E"
        panel_height: 28 
{% endfor %}

{{ HOME }}/bin/blurlock.sh:
  file.managed:
    - source: salt://files/blurlock.sh
    - template: jinja
    - user: aesdana
    - group: aesdana
    - mode: 755
    - makedirs: true
    - defaults:
        fg: "A7ADBA"
        bg: "2B303B"
        accent: "A3BE8C"
        urgent: "AD5860"
        yellow: "EBCB8B"
{% for mode in "home","work" %}
{{ HOME }}/.tmuxinator/{{ mode }}.yml:
  file.managed:
    - source: salt://files/tmuxinator.{{ mode }}.yml
    - template: jinja
    - user: aesdana
    - group: aesdana
{% endfor %}
